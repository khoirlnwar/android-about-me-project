package com.example.aboutmeproject

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.example.aboutmeproject.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding
    private val myName: MyName = MyName("Khoirul Anwr")

    lateinit var textViewName: TextView
    lateinit var doneButton: Button
    lateinit var editTextNickname: EditText
    lateinit var textViewNickname: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.myNameKhoirul = myName

        initialComponents()

        doneButton.setOnClickListener {
            onDoneButton(it)
        }
    }

    private fun onDoneButton(view: View) {
        binding.apply {
            // get text from edit text
//            textViewNickname.text = editTextNickname.text
            binding.myNameKhoirul?.nickname = editTextNickname.text.toString();

            // hide edit text
            editTextNickname.visibility = View.GONE

            // hide button
            view.visibility = View.GONE

            // un-hide nickname textview
            textViewNickname.visibility = View.VISIBLE

            // request to refresh UI
            invalidateAll()
        }


        // hide the keyboard
        hideKeyboard(view)
    }

    private fun initialComponents() {
        // textview name in top
//        textViewName = findViewById(R.id.name_textView)
        textViewName = binding.nameTextView

        // done button
//        doneButton = findViewById(R.id.done_button)
        doneButton = binding.doneButton


        // edit text name
//        editTextNickname = findViewById(R.id.nickname_edit)
        editTextNickname = binding.nicknameEdit

        // text view nickname under button
//        textViewNickname = findViewById(R.id.nicknam_textview)
        textViewNickname = binding.nicknamTextview
    }

    private fun hideKeyboard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}